from datetime import datetime

from mongoengine.errors import NotUniqueError
from flask import (
    Blueprint,
    render_template,
    redirect,
    g,
    request,
    url_for,
)
from project.services.mongo.models import Test, TestRun
from .decorators import login_required
from .common.utils import calculate_score

test_bp = Blueprint('main_page', __name__, template_folder='templates')


@test_bp.route('/')
@login_required
def tests():
    tests = Test.objects
    return render_template('tests.html', tests=tests)


@test_bp.route('/test/description/<int:slug>/', methods=('GET', 'POST'))
@login_required
def test_description(slug):
    test = Test.objects(slug=slug).first()

    if request.method == 'POST':
        run_id = TestRun.objects.count() + 1
        test_run = TestRun(
            run_id=run_id,
            user=g.user,
            test=test,
            start_date=datetime.now(),
        )

        try:
            test_run.save()
        except NotUniqueError:
            return redirect(url_for('main_page.tests'))

        return render_template('test_run.html', test=test, test_run=test_run)

    return render_template('test_description.html', test=test)


@test_bp.route('/test/run/<int:slug>/')
@login_required
def test_run(slug):
    test = Test.objects(slug=slug).first()

    return render_template('test_description.html', test=test)


@test_bp.route('/test/end/<int:run_id>/', methods=('GET', 'POST'))
@login_required
def test_end(run_id):
    test_run = TestRun.objects(run_id=run_id).first()

    if request.method == 'POST':
        test_run.finish_date = datetime.now()

        test_run.score = calculate_score(test_run, request.form)
        test_run.save()

    return render_template('test_end.html', test_run=test_run)
