from marshmallow import Schema, fields, post_load
from werkzeug.security import generate_password_hash


class UserSchema(Schema):
    username = fields.Str(validate=lambda value: len(value) > 0, required=True)
    password = fields.Str(validate=lambda value: len(value) > 0, required=True)

    @post_load
    def make_user(self, data):
        password = generate_password_hash(data['password'])
        username = data['username']
        user_data = {
                        'username': username,
                        'password': password,
                    }
        return user_data


class TestSchema(Schema):
    pass
