from flask import g
from mongoengine import connect

from project.services.mongo.user_service import UserServiceMongo
from project.services.sqla.user_service import UserServiceAlchemy
from .config import MONGO_URI, SQLA_URI


def get_sqla_service():
    if 'user_service' not in g:
        print('connected sqla')

        user_service = UserServiceAlchemy({
            'URI': SQLA_URI,
        })
        user_service.connect()

        g.user_service = user_service
    return g.user_service


def get_mongo_service():
    if 'user_service' not in g:
        print('connected mongo')

        user_service = UserServiceMongo({
            'URI': MONGO_URI,
        })
        user_service.connect()

        g.user_service = user_service
    return g.user_service


def connect_to_mongo():
    connect(host=MONGO_URI)


def close_service(e=None):
    user_service = g.pop('user_service', None)

    if user_service is not None:
        print('disconnected')

        user_service.disconnect()
