from flask import (
    Blueprint, g, redirect, render_template, request, session, url_for
)
from werkzeug.security import check_password_hash
from marshmallow import ValidationError
from mongoengine.errors import NotUniqueError


from .validate import UserSchema


auth_bp = Blueprint('auth', __name__, url_prefix='/auth')


@auth_bp.route('/register', methods=('GET', 'POST'))
def register():
    user_service = g.user_service
    if request.method == 'POST':
        try:
            user_data = UserSchema().load(request.form)
        except ValidationError as err:
            return render_template('auth/register.html', errors=err.messages)
        user = user_service.create_user(**user_data)
        
        try:
            user_service.save(user)
        except NotUniqueError:
            return render_template('auth/register.html', errors={
                'IntegrityError': ['this username is used', ],
            })

        return redirect(url_for('auth.login'))

    return render_template('auth/register.html', errors={})


@auth_bp.route('/login', methods=('GET', 'POST'))
def login():
    user_service = g.user_service
    
    if request.method == 'POST':
        errors = UserSchema().validate(request.form)
        if errors:
            return render_template('auth/login.html', errors=errors)

        user = user_service.get_by_username(request.form['username'])
        print(user)
        if user and check_password_hash(
                user.password,
                request.form['password'],
        ):
            session.clear()
            session['user_id'] = str(user.user_id)
            return redirect(url_for('main_page.tests'))

    return render_template('auth/login.html', errors={})


@auth_bp.route('/logout')
def logout():
    session.clear()
    return redirect(url_for('auth.login'))
