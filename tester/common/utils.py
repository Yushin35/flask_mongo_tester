

def calculate_score(test_run, form):
    score = 0
    questions = test_run.test.questions.items()
    for question_id, question in questions:
        for answer_id, answer in question.answers.items():
            if answer.value == form.get(question_id) and answer.right:
                score += 1
    score = (score / len(questions)) * 100
    return score
