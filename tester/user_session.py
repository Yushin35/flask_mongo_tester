from flask import g, session


def load_logged_in_user():
    user_id = session.get('user_id')
    if user_id is None:
        g.user = None
    else:
        g.user = g.user_service.get_by_id(user_id)
