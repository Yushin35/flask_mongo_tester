import os

from flask import Flask, g

from .test_bp import test_bp
from .auth_bp import auth_bp

from .config import SECRET_KEY, MONGO_URI, DEBUG
from .db_service import close_service, get_mongo_service, get_sqla_service, connect_to_mongo
from .user_session import load_logged_in_user


def create_app():
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_mapping(
        SECRET_KEY=SECRET_KEY,
        DEBUG=DEBUG
    )

    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    app.register_blueprint(test_bp)
    app.register_blueprint(auth_bp)
    app.teardown_appcontext(close_service)

    @app.before_request
    def before_request():
        # get_mongo_service()
        get_sqla_service()
        connect_to_mongo()
        load_logged_in_user()

    return app
