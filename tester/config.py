import os

from dotenv import load_dotenv
from flask_mongoengine import MongoEngine


APP_ROOT = os.path.join(os.path.dirname(__file__), '..')
dotenv_path = os.path.join(APP_ROOT, '.env')
load_dotenv(dotenv_path)


DEBUG = bool(os.getenv('DEBUG'))
MONGO_URI = os.getenv('FLASK_MONGO_URI')
SQLA_URI = os.getenv('FLASK_SQLA_URI')

SECRET_KEY = os.getenv('FLASK_SECRET_KEY')
